import React from 'react';
import '../assets/styles/TodoList.css'


export class TodoList extends React.Component {
  render(){
    return(
      <div>
        <h1>{this.props.children}</h1>
        {this.props.todos.map( item => {
          return(
            <div key={item.id}>
              <h1 onClick={()=>this.props.toggleComplete(item.id)} className={item.completed ? "merah" : "biru" }>{item.title}</h1>
              <p>{item.id}</p>
              <button onClick={()=>this.props.delTodo(item.id)}>delete</button>
            </div>
          )
        })}
      </div>
    )
  }
}