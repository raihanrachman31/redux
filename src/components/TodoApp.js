import React from 'react';
import {TodoList} from './TodoList';
import { AddTodo } from './AddTodo';

export class TodoApp extends React.Component {
  state = {
    todos: [
      {
        id: 1,
        title: "quis ut nam facilis et officia qui",
        completed: false
      },
      {
        id: 2,
        title: "deletus anus islum facis komunis libris",
        completed: true
      }
    ],
    test: "halo"
  }

  addTask = async (data) => {
    await this.setState({
      todos: [...this.state.todos, data]
    })
    console.log(this.state.todos)
  }

  test = ()=> {
    this.setState({
      test: 'hey'
    })
  }
  
delTodo = id => {
  this.setState({
    todos: this.state.todos.filter( todo => todo.id !== id)
  })
  

}

  toggleComplete = async id => {
    this.setState({
      todos: this.state.todos.map (todo => {
        if(todo.id === id) {
          if(todo.completed){
            return {...todo,completed: false}
          } else {
            return {...todo,completed: true}
          }
        } else {
          return todo
        }
      })
    
    // const data = await this.state.todos.find( item => {
    //   item.id === id return {
    //     ...item, completed: true
    //   }
    // }
    
     
     })
  }
  render(){
    return(
      <div>
        <h1>Todo List</h1>
        <AddTodo todos={this.state.todos} addTask={this.addTask}/>
        <TodoList toggleComplete={this.toggleComplete} delTodo={this.delTodo} todos={this.state.todos} testFunction={this.test} testText={this.state.text}>child</TodoList>
      </div>
      
    )
  }
}