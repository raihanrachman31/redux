import React from 'react'

export class AddTodo extends React.Component {
  state = {
    title:"",
  }

  onChange = e => {
    this.setState({
      title: e.target.value,
    })
  }

  onSubmit = e => {
    e.preventDefault()
    const newTodo = {
      id: this.props.todos.length+1,
      title:this.state.title,
      completed: false
    }
    this.props.addTask(newTodo)
  }
  render(){
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <input onChange={this.onChange} type="text" name="" id="todo-input" value={this.title}/>
          <button>+</button>
        </form>
      </div>
    )
  }
}
